//
//  FakeNetworkRequestManager.swift
//  Urbi_Et_Orbi_Tests
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
@testable import Urbi_Et_Orbi


class FakeNetworkRequestManager: NetworkRequestManagerProtocol {
    var json: Data?
    var error: Error?
    var errorJson: Data?
    
    func dataRequest(_ url: URL, success: @escaping (Data?) -> Void, failure: @escaping (Error, Data?) -> Void) -> NetworkTask? {
        let task = FakeNetworkTask()
        
        if let error = error {
            task.failure = failure
            task.error = error
            
            if let errorJson = errorJson {
                task.errorJson = errorJson
            }
        } else {
            task.success = success
            task.json = json
        }
        
        return task
    }
}
