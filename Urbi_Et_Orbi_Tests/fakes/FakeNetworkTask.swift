//
//  FakeNetworkTask.swift
//  Urbi_Et_Orbi_Tests
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
@testable import Urbi_Et_Orbi

class FakeNetworkTask: NetworkTask {
    var json: Data?
    var error: Error?
    var errorJson: Data?
    
    var success: ((Data?) -> Void)?
    var failure: ((Error, Data?) -> Void)?
    
    func start() {
        if let success = success {
            success(json)
        } else {
            if let failure = failure {
                failure(error!, errorJson)
            }
        }
    }
    func cancel() {}
    
}
