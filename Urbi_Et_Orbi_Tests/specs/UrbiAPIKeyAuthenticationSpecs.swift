//
//  Urbi_Et_Orbi_Tests.swift
//  Urbi_Et_Orbi_Tests
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import XCTest
@testable import Urbi_Et_Orbi

class Urbi_Et_Orbi_Tests: XCTestCase {

    func test_urbi_api_authentication_adapt_request() {
        let apiKey = "ABCDE12345"
        let auth = UrbiAPIKeyAuthentication(apiKey: apiKey)
        let requestURL = URL(string: "https://someURL")!
        var request = URLRequest(url: requestURL)
        
        auth.adaptRequest(&request)
        
        guard let headers = request.allHTTPHeaderFields else { XCTFail() ; return }
        
        XCTAssertEqual(headers["x-api-key"], apiKey)
    }

}
