//
//  UrbiAPIServiceSpecs.swift
//  Urbi_Et_Orbi_Tests
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import XCTest
@testable import Urbi_Et_Orbi

class UrbiAPIServiceSpecs: XCTestCase {
    
    func test_vehicles_success() {
        let json = """
        {
            "cache": {
                "sharengo": true,
                "mimoto": false
            },
            "location": "milano",
            "vehicles": [
                {
                "fuel": {
                    "percent": 99,
                    "range": 49,
                    "type": "electric"
                },
                "id": "5cb5efeb1870c60011727f39",
                "imageUrl": "mimoto_scooter",
                "licensePlate": "X88DLJ",
                "location": {
                    "lat": 45.449908,
                    "lon": 9.195852
                },
                "model": "Askoll eS2",
                "pinUrl":   "https://s3-eu-west-1.amazonaws.com/static.urbi.co/pins/p/pin_mimoto.png",
                "prices": {
                    "currency": "eur",
                    "daily": [
                    {
                        "amount": 1990,
                        "unit": "day"
                    }
                    ],
                    "moving": [
                    {
                        "amount": 26,
                        "unit": "min"
                    },
                    {
                        "amount": 490,
                        "unit": "hour"
                    }
                    ],
                    "parked": [
                    {
                        "amount": 26,
                        "unit": "min"
                    }
                    ]
                },
                "provider": "mimoto",
                "seats": 2,
                "transmission": "automatic",
                "type": "scooter",
                "label": "134"
                },
                {
                "fuel": {
                    "percent": 96,
                    "range": 48,
                    "type": "electric"
                },
                "id": "5cb5f1791870c60011728f36",
                "imageUrl": "mimoto_scooter",
                "licensePlate": "X88DH5",
                "location": {
                    "lat": 45.488806,
                    "lon": 9.193505
                },
                "model": "Askoll eS2",
                "pinUrl": "https://s3-eu-west-1.amazonaws.com/static.urbi.co/pins/p/pin_mimoto.png",
                "prices": {
                    "currency": "eur",
                    "daily": [
                    {
                        "amount": 1990,
                        "unit": "day"
                    }
                    ],
                    "moving": [
                    {
                        "amount": 26,
                        "unit": "min"
                    },
                    {
                        "amount": 490,
                        "unit": "hour"
                    }
                    ],
                    "parked": [
                    {
                        "amount": 26,
                        "unit": "min"
                    }
                    ]
                },
                "provider": "mimoto",
                "seats": 2,
                "transmission": "automatic",
                "type": "scooter",
                "label": "149"
                }
            ]
            }
        """.data(using: .utf8)
        
        let networkManager = FakeNetworkRequestManager()
        networkManager.json = json
        
        let apiService = UrbiApiService(networkManger: networkManager)
        
        let exp = expectation(description: "")
        
        apiService.vehicles(city: .milano, providers: [.mimoto, .sharengo], success: { vehicles in
            XCTAssertEqual(vehicles.location, "milano")
            
            XCTAssertEqual(vehicles.vehicles.count, 2)
            
            XCTAssertEqual(vehicles.vehicles.first?.fuel.percent, 99)
            XCTAssertEqual(vehicles.vehicles.first?.fuel.range, 49)
            XCTAssertEqual(vehicles.vehicles.first?.fuel.type, FuelType.electric)
            XCTAssertEqual(vehicles.vehicles.first?.id, "5cb5efeb1870c60011727f39")
            XCTAssertEqual(vehicles.vehicles.first?.imageUrl, "mimoto_scooter")
            XCTAssertEqual(vehicles.vehicles.first?.licensePlate, "X88DLJ")
            XCTAssertEqual(vehicles.vehicles.first?.location.lat, 45.449908)
            XCTAssertEqual(vehicles.vehicles.first?.location.lon, 9.195852)
            XCTAssertEqual(vehicles.vehicles.first?.model, "Askoll eS2")
            XCTAssertEqual(vehicles.vehicles.first?.pinUrl, "https://s3-eu-west-1.amazonaws.com/static.urbi.co/pins/p/pin_mimoto.png")
            XCTAssertEqual(vehicles.vehicles.first?.type, VehicleType.scooter)
            
            XCTAssertEqual(vehicles.vehicles.last?.fuel.percent, 96)
            XCTAssertEqual(vehicles.vehicles.last?.fuel.range, 48)
            XCTAssertEqual(vehicles.vehicles.last?.fuel.type, FuelType.electric)
            XCTAssertEqual(vehicles.vehicles.last?.id, "5cb5f1791870c60011728f36")
            XCTAssertEqual(vehicles.vehicles.last?.imageUrl, "mimoto_scooter")
            XCTAssertEqual(vehicles.vehicles.last?.licensePlate, "X88DH5")
            XCTAssertEqual(vehicles.vehicles.last?.location.lat, 45.488806)
            XCTAssertEqual(vehicles.vehicles.last?.location.lon, 9.193505)
            XCTAssertEqual(vehicles.vehicles.last?.model, "Askoll eS2")
            XCTAssertEqual(vehicles.vehicles.last?.pinUrl, "https://s3-eu-west-1.amazonaws.com/static.urbi.co/pins/p/pin_mimoto.png")
            XCTAssertEqual(vehicles.vehicles.first?.type, VehicleType.scooter)
            exp.fulfill()
            
        }, failure: { _ in
            XCTFail()
            exp.fulfill()
        })
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func test_vehicles_failure_jsonError() {
        let errorJson = """
            {
              "message": "No enum constant co.urbi.model.provider.Provider.airplane",
              "status": 400,
              "statusFromProvider": false,
              "uri": "/location/milano/vehicle/airplane"
            }
        """.data(using: .utf8)
        
        let networkManager = FakeNetworkRequestManager()
        networkManager.error = NetworkError.httpError(400)
        networkManager.errorJson = errorJson
        
        let apiService = UrbiApiService(networkManger: networkManager)
        
        let exp = expectation(description: "")
        
        apiService.vehicles(city: .milano, providers: [.mimoto], success: { _ in
            XCTFail()
            exp.fulfill()
        }) { error in
            guard let urbierror = error as? UrbiError else {
                XCTFail()
                exp.fulfill()
                return
            }
            XCTAssertEqual(urbierror.message, "No enum constant co.urbi.model.provider.Provider.airplane")
            XCTAssertEqual(urbierror.status, 400)
            XCTAssertFalse(urbierror.statusFromProvider)
            XCTAssertEqual(urbierror.uri, "/location/milano/vehicle/airplane")
            exp.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
}


