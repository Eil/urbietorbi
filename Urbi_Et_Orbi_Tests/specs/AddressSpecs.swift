//
//  AddressSpecs.swift
//  Urbi_Et_Orbi_Tests
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import XCTest
@testable import Urbi_Et_Orbi


class AddressSpecs: XCTestCase {
    
    func test_address_all_values() {
        let address = Address(street: "Via Bellissima", number: "1", zipCode: "00000", locality: "Posto Fantastico")
        let expectedString = "Via Bellissima, 1, 00000 Posto Fantastico"
        
        XCTAssertEqual(address.defaultFormatting(), expectedString)
    }
    
    func test_address_no_values() {
        let address = Address(street: nil, number: nil, zipCode: nil, locality: nil)
        
        XCTAssertTrue(address.defaultFormatting().isEmpty)
    }
    
    func test_address_no_street() {
        let address = Address(street: nil, number: nil, zipCode: "00000", locality: "Posto Fantastico")
        let expectedString = "00000 Posto Fantastico"
        
        XCTAssertEqual(address.defaultFormatting(), expectedString)
    }
}
