//
//  URL+QueryParametersSpecs.swift
//  Urbi_Et_Orbi_Tests
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import XCTest
@testable import Urbi_Et_Orbi

class UrbiApiRequestURLSpecs : XCTestCase {
    
    func test_urbiApiUrl_singleCompany() {
        let vehiclesURL = UrbiApiRequestURL.vehicles(version: .v1, city: .milano, providers: [.mimoto])
        
        let stringURL = "https://p.urbi.co/v1/location/milano/vehicle/mimoto"
        let expectedURL = URL(string: stringURL)
        
        XCTAssertEqual(vehiclesURL, expectedURL)
    }
    
    func test_urbiApiUrl_multipleCompany() {
        let vehiclesURL = UrbiApiRequestURL.vehicles(version: .v1, city: .milano, providers: [.mimoto, .sharengo])
        
        let stringURL = "https://p.urbi.co/v1/location/milano/vehicle/mimoto,sharengo"
        let expectedURL = URL(string: stringURL)
        
        XCTAssertEqual(vehiclesURL, expectedURL)
    }
}
