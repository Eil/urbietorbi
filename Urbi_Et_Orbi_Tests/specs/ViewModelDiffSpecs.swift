//
//  ViewModelDiffSpecs.swift
//  Urbi_Et_Orbi_Tests
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import XCTest
import DeepDiff
@testable import Urbi_Et_Orbi


class ViewModelDiffSpecs: XCTestCase {
    
    func test_viewModelDiff_adds() {
        let vehicle1 = Vehicle(fuel: Fuel(percent: 10, range: 10, type: .electric),
                               id: "1001",
                               imageUrl: nil,
                               licensePlate: "A",
                               location: Location(lat: 101, lon: 101),
                               model: "model1",
                               pinUrl: "pinUrl1",
                               provider: .sharengo,
                               type: .scooter)
        
        let vehicle2 = Vehicle(fuel: Fuel(percent: 20, range: 20, type: .electric),
                               id: "1002",
                               imageUrl: nil,
                               licensePlate: "B",
                               location: Location(lat: 102, lon: 102),
                               model: "model2",
                               pinUrl: "pinUrl2",
                               provider: .mimoto,
                               type: .car)
        
        let vehicle3 = Vehicle(fuel: Fuel(percent: 30, range: 30, type: .electric),
                               id: "1003",
                               imageUrl: nil,
                               licensePlate: "C",
                               location: Location(lat: 103, lon: 103),
                               model: "model3",
                               pinUrl: "pinUrl3",
                               provider: .sharengo,
                               type: .scooter)
        
        let oldModels = [vehicle1, vehicle2].map({ VehicleAnnotationViewModel(vehicle: $0)})
        let newModels = [vehicle1, vehicle2, vehicle3].map({ VehicleAnnotationViewModel(vehicle: $0)})
        
        let diff = ViewModelDiff<VehicleAnnotationViewModel>(from: oldModels, to: newModels)
        
        XCTAssertEqual(diff.removes, [])
        XCTAssertEqual(diff.adds, [newModels.last!])
    }
    
    func test_viewModelDiff_removes() {
        let vehicle1 = Vehicle(fuel: Fuel(percent: 10, range: 10, type: .electric),
                               id: "1001",
                               imageUrl: nil,
                               licensePlate: "A",
                               location: Location(lat: 101, lon: 101),
                               model: "model1",
                               pinUrl: "pinUrl1",
                               provider: .sharengo,
                               type: .scooter)
        
        let vehicle2 = Vehicle(fuel: Fuel(percent: 20, range: 20, type: .electric),
                               id: "1002",
                               imageUrl: nil,
                               licensePlate: "B",
                               location: Location(lat: 102, lon: 102),
                               model: "model2",
                               pinUrl: "pinUrl2",
                               provider: .mimoto,
                               type: .car)
        
        let vehicle3 = Vehicle(fuel: Fuel(percent: 30, range: 30, type: .electric),
                               id: "1003",
                               imageUrl: nil,
                               licensePlate: "C",
                               location: Location(lat: 103, lon: 103),
                               model: "model3",
                               pinUrl: "pinUrl3",
                               provider: .sharengo,
                               type: .scooter)
        
        let oldModels = [vehicle1, vehicle2, vehicle3].map({ VehicleAnnotationViewModel(vehicle: $0)})
        let newModels = [vehicle1, vehicle3].map({ VehicleAnnotationViewModel(vehicle: $0)})
        
        let diff = ViewModelDiff<VehicleAnnotationViewModel>(from: oldModels, to: newModels)
        
        XCTAssertEqual(diff.adds, [])
        XCTAssertEqual(diff.removes, [oldModels[1]])
    }
    
    func test_viewModelDiff_adds_and_removes() {
        let vehicle1 = Vehicle(fuel: Fuel(percent: 10, range: 10, type: .electric),
                               id: "1001",
                               imageUrl: nil,
                               licensePlate: "A",
                               location: Location(lat: 101, lon: 101),
                               model: "model1",
                               pinUrl: "pinUrl1",
                               provider: .sharengo,
                               type: .scooter)
        
        let vehicle2 = Vehicle(fuel: Fuel(percent: 20, range: 20, type: .electric),
                               id: "1002",
                               imageUrl: nil,
                               licensePlate: "B",
                               location: Location(lat: 102, lon: 102),
                               model: "model2",
                               pinUrl: "pinUrl2",
                               provider: .mimoto,
                               type: .car)
        
        let vehicle3 = Vehicle(fuel: Fuel(percent: 30, range: 30, type: .electric),
                               id: "1003",
                               imageUrl: nil,
                               licensePlate: "C",
                               location: Location(lat: 103, lon: 103),
                               model: "model3",
                               pinUrl: "pinUrl3",
                               provider: .sharengo,
                               type: .scooter)
        
        let vehicle4 = Vehicle(fuel: Fuel(percent: 40, range: 40, type: .electric),
                               id: "1004",
                               imageUrl: nil,
                               licensePlate: "D",
                               location: Location(lat: 104, lon: 104),
                               model: "model4",
                               pinUrl: "pinUrl4",
                               provider: .mimoto,
                               type: .car)
        
        let oldModels = [vehicle1, vehicle2, vehicle3].map({ VehicleAnnotationViewModel(vehicle: $0)})
        let newModels = [vehicle1, vehicle3, vehicle4].map({ VehicleAnnotationViewModel(vehicle: $0)})
        
        let diff = ViewModelDiff<VehicleAnnotationViewModel>(from: oldModels, to: newModels)
        
        XCTAssertEqual(diff.adds, [newModels.last!])
        XCTAssertEqual(diff.removes, [oldModels[1]])
    }
}
