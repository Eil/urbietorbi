//
//  Authentication.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


protocol Authentication {
    func adaptRequest(_ request: inout URLRequest)
}

class UrbiAPIKeyAuthentication: Authentication {
    private let apiKey: String
    private let headerKey: String = "x-api-key"
    
    init(apiKey: String) {
        self.apiKey = apiKey
    }
    
    func adaptRequest(_ request: inout URLRequest) {
        request.addValue(apiKey, forHTTPHeaderField: self.headerKey)
    }
}
