//
//  NetworkError.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


enum NetworkError: Error {
    case generic
    case taskError(Error)
    case httpError(Int)
    
    var localizedDescription: String {
        get {
            switch self {
            case .generic:
                return "Something went wrong"
            case .taskError(let error):
                return "Task error: \(error.localizedDescription)"
            case .httpError(let statusCode):
                return "Http error: \(statusCode)"
            }
        }
    }
}
