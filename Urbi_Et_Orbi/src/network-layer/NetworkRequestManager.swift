//
//  APIManager.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


protocol NetworkRequestManagerProtocol  {
    func dataRequest(_ url: URL, success: @escaping (Data?) -> Void, failure: @escaping (Error, Data?) -> Void) -> NetworkTask?
}

class NetworkRequestManager: NetworkRequestManagerProtocol {
    
    static let `default` = NetworkRequestManager(authentication: UrbiAPIKeyAuthentication(apiKey: "Nv3day0hzV3GZ0JceINKM8LxYfcQc0k346paK8L7"))
    
    private let session: URLSession
    private let authentication: Authentication?
    
    init(_ session: URLSession = URLSession.shared, authentication: Authentication? = nil) {
        self.session = session
        self.authentication = authentication
    }
    
    @discardableResult
    func dataRequest(_ url: URL, success: @escaping (Data?) -> Void, failure: @escaping (Error, Data?) -> Void) -> NetworkTask? {
        
        var request = URLRequest(url: url)
        authentication?.adaptRequest(&request)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            if let taskerror = error {
                failure(NetworkError.taskError(taskerror), nil)
            } else if let response = response as? HTTPURLResponse, response.isError() {
                failure(NetworkError.httpError(response.statusCode), data)
            } else {
                success(data)
            }
        }
        
        return URLSessionNetworkTask(task: task)
    }
}
