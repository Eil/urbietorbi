//
//  Task.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation

protocol NetworkTask {
    func start()
    func cancel()
}

class URLSessionNetworkTask: NetworkTask {
    
    private weak var task: URLSessionTask?
    
    init(task: URLSessionTask) {
        self.task = task
    }
    
    func start() {
        task?.resume()
    }
    
    func cancel() {
        task?.cancel()
    }
}
