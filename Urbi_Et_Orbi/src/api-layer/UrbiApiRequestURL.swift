//
//  URBIAPIService.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


class UrbiApiRequestURL {
    
    enum Version: String {
        case v1
    }
    
    private static let scheme = "https"
    private static let host   = "p.urbi.co"
    private static let locationService = "location"
    private static let vehicleService = "vehicle"
    
    private let networkRequestManager: NetworkRequestManagerProtocol
    
    init(networkRequestManager: NetworkRequestManagerProtocol) {
        self.networkRequestManager = networkRequestManager
    }
    
    class func vehicles(version: UrbiApiRequestURL.Version, city: Urbi.City, providers: [Urbi.Provider]) -> URL? {
        var components = URLComponents()
        components.scheme = UrbiApiRequestURL.scheme
        components.host = UrbiApiRequestURL.host
        
        let versionPath = version.rawValue
        let locationPath = UrbiApiRequestURL.locationPath(city: city)
        let vehiclePath = UrbiApiRequestURL.vehiclePath(providers: providers)
        
        components.path = "/\(versionPath)/\(locationPath)/\(vehiclePath)"
        return components.url
    }
    
    private class func locationPath(city: Urbi.City) -> String {
        return "\(UrbiApiRequestURL.locationService)/\(city.rawValue)"
    }
    
    private class func vehiclePath(providers: [Urbi.Provider]) -> String {
        var companies: String = ""
        
        if !providers.isEmpty {
            companies.append(contentsOf: providers.compactMap({$0.rawValue}).joined(separator: ","))
        }
        
        return "\(UrbiApiRequestURL.vehicleService)/\(companies)"
    }
}
