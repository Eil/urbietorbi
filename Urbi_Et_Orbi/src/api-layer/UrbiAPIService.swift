//
//  UrbiAPIService.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation

typealias VehiclesSuccessCallback = (Vehicles) -> Void
typealias ErrorCallback = (Error) -> Void

enum UrbiApiServiceError : Error {
    case invalidURL
    case decodingError(Error)
    
    var localizedDescription: String {
        get {
            switch self {
            case .invalidURL:
                return "invalid URL"
            case .decodingError(let error):
                return "error decodingJson: \(error.localizedDescription)"
            }
        }
    }
}

class UrbiApiService {
    
    private let networkManager: NetworkRequestManagerProtocol
    
    var task: NetworkTask?
    
    init(networkManger: NetworkRequestManagerProtocol) {
        self.networkManager = networkManger
    }
    
    static let `default` = UrbiApiService(networkManger: NetworkRequestManager.default)
    
    func vehicles(city: Urbi.City, providers: [Urbi.Provider], success: @escaping VehiclesSuccessCallback, failure: @escaping ErrorCallback) {
        guard let requestURL = UrbiApiRequestURL.vehicles(version: .v1, city: city, providers: providers) else {
            failure(UrbiApiServiceError.invalidURL)
            return
        }
        self.task = networkManager.dataRequest(requestURL, success: { responsedata in
            guard let data = responsedata else {
                failure(UrbiApiServiceError.invalidURL)
                self.task = nil
                return
            }
            do {
                let json: Vehicles = try data.decode()
                success(json)
                self.task = nil
            } catch let error {
                failure(UrbiApiServiceError.decodingError(error))
                self.task = nil
            }
        }, failure: { error, errordata in
            guard let data = errordata else {
                failure(error)
                self.task = nil
                return
            }
            if let errorJson: UrbiError = try? data.decode() {
                failure(errorJson)
            } else {
                failure(error)
            }
            self.task = nil
        })
        
        task?.start()
    }
    
    func cancel() {
        self.task?.cancel()
        self.task = nil
    }
}
