//
//  Screens.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit


class Screens {
    
    class func createHomeController() -> UIViewController {
        let tabbar = UITabBarController()
        UITabBar.appearance().tintColor = Colors.accentColor
        UITabBar.appearance().barTintColor = .white
        UITabBar.appearance().isTranslucent = false
        
        let mapView = vehiclesMapView()
        mapView.tabBarItem.title = "home_controller_map".localized()
        mapView.tabBarItem.image = "tab_bar_map".icon()
        
        let listView = vehiclesListView()
        listView.tabBarItem.title = "home_controller_list".localized()
        listView.tabBarItem.image = "tab_bar_list".icon()
        
        tabbar.viewControllers = [mapView, listView]
        
        return tabbar
    }
    
    private class func vehiclesMapView() -> UIViewController {
        let mobility = Mobility.milan()
        let dp = VehiclesDataProvider(mobility: mobility)
        let interactor = VehiclesPOIMapInteractor(dataProvider: dp, city: mobility.city)
        
        let presenter = VehiclesPOIMapPresenter(interactor: interactor)
        interactor.presenter = presenter
        
        let view = VehiclesPOIMapView()
        presenter.view = view
        
        view.presenter = presenter
        
        return view
    }
    
    private class func vehiclesListView() -> UIViewController {
        let mobility = Mobility.milan()
        let dp = VehiclesDataProvider(mobility: mobility)
        
        let interactor = VehiclesListInteractor(dataProvider: dp)
        
        let presenter = VehiclesListPresenter(interactor: interactor)
        interactor.presenter = presenter
        
        let view = VehiclesListView()
        presenter.view = view
        
        view.presenter = presenter
        
        return view
    }
}
