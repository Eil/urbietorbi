//
//  VehiclesPOIMapDataProvider.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


protocol DataProvider {
    func fetch<T>(_ completion: @escaping (FetchResult<T>) -> Void)
    func cancelFetch()
}

enum InternalError: Error {
    case generic
}

class VehiclesDataProvider: DataProvider {
    let mobility: Mobility
    
    init(mobility: Mobility) {
        self.mobility = mobility
    }
    
    private lazy var service: UrbiApiService = UrbiApiService.default
    
    func fetch<T>(_ completion: @escaping (FetchResult<T>) -> Void) {
        fetchVehicles(in: self.mobility.city, companies: self.mobility.providers, success: { vehicles in
            guard let vehicles = vehicles as? T else {
                completion(.error(InternalError.generic))
                return
            }
            completion(.success(vehicles))
        }, failure: { error in
            completion(.error(error))
        })
    }
    
    func cancelFetch() {
        service.cancel()
    }
    
    private func fetchVehicles(in city: Urbi.City, companies: [Urbi.Provider], success: @escaping VehiclesSuccessCallback, failure: @escaping ErrorCallback) {
        service.vehicles(city: city, providers: companies, success: { vehicles in
            success(vehicles)
        }, failure: { error in
            failure(error)
        })
    }
}
