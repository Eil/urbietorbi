//
//  VehiclesListInteractor.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit


class VehiclesListInteractor: Interactor {
    let dataProvider: DataProvider
    
    var currentLocation: CLLocation?
    weak var presenter: VehiclesListPresenter?
    
    init(dataProvider: DataProvider) {
        self.dataProvider = dataProvider
    }
    
    func fetch() {
        LocationManager.shared.getCurrentLocation { [weak self] location, locationerror in
            guard locationerror == nil else {
                self?.onLocationError(locationerror!)
                return
            }
            guard let currentLocation = location, let sself = self else {
                self?.presenter?.showErrorMessage(nil)
                return
            }
            sself.currentLocation = currentLocation
            sself.fetchVehicles()
        }
    }
    
    func cancelFetch() {
        dataProvider.cancelFetch()
    }
    
    private func fetchVehicles() {
        dataProvider.fetch { [weak self] (fetchresult: FetchResult<Vehicles>) in
            guard let sself = self else { return }
            
            switch fetchresult {
            case .success(let vehicles):
                let viewModels = vehicles.vehicles.map({ VehicleListViewModelFactory.model(from: $0, considering: sself.currentLocation) })
                let sortedModels = sself.sorted(viewModels: viewModels)
                sself.presenter?.didRetrieve(sortedModels)
            case .error(let error):
                guard let taskError = error as? NetworkError else {
                    sself.presenter?.showErrorMessage()
                    return
                }
                sself.onNetworkError(taskError)
            }
        }
    }
    
    private func sorted(viewModels: [VehicleListViewModel]) -> [VehicleListViewModel] {
        let sortedModels = viewModels.sorted { (model1, model2) -> Bool in
            guard let distance1 = model1.distance, let distance2 = model2.distance else { return false }
            let rawDistance1 = MKDistanceFormatter().distance(from: distance1)
            let rawDistance2 = MKDistanceFormatter().distance(from: distance2)
            
            return rawDistance1 < rawDistance2
        }
        
        return sortedModels
    }
    
    private func onNetworkError(_ error: NetworkError) {
        switch error {
        case .taskError(let foundationError):
            guard (foundationError as NSError).code != NSURLErrorCancelled else { return }
            self.presenter?.showErrorMessage()
        default:
            self.presenter?.showErrorMessage()
        }
    }
    
    private func onLocationError(_ error: LocationError) {
        switch error {
        case .unauthorized:
            self.presenter?.showErrorMessage("location_error_permission_message".localized())
        default:
            self.presenter?.showErrorMessage("location_error_generic_message".localized())
        }
    }
}
