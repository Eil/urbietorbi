//
//  VehiclesListView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit
import DeepDiff


class VehiclesListView: UIViewController {
    
    var presenter: VehiclesListPresenter?
    
    lazy private var vehiclesList: UITableView = {
        let tableView = UITableView()
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.tableFooterView = UIView(frame: CGRect.zero)
        tableView.delegate = self.presenter
        tableView.dataSource = self.presenter
        tableView.register(VehiclesListCell.self, forCellReuseIdentifier: VehiclesListCell.identifier)
        tableView.backgroundColor = .white
        tableView.separatorColor = .darkGray
        
        return tableView
    }()
    
    lazy var loadingView: LoadingView = {
        let l = LoadingView()
        l.backgroundColor = .white
        l.translatesAutoresizingMaskIntoConstraints = false
        
        return l
    }()
    
    lazy var refreshControl: UIRefreshControl = {
        let r = UIRefreshControl()
        r.addTarget(self, action: #selector(didPullToRefresh), for: .valueChanged)
        
        return r
    }()
    
    // MARK:- View controller life-cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.fetchData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter?.stopFetch()
    }
    
    // MARK:- Public APIs
    func reload(changes: [Change<VehicleListViewModel>]) {
        DispatchQueue.main.async {
            self.refreshDidEnd()
            self.vehiclesList.reload(changes: changes, updateData: {})
        }
    }
    
    func showLoadingView() {
        DispatchQueue.main.async {
            self.loadingView.isHidden = false
            self.vehiclesList.isHidden = true
            self.loadingView.startLoading()
        }
    }
    
    func showListView() {
        DispatchQueue.main.async {
            self.loadingView.isHidden = true
            self.vehiclesList.isHidden = false
        }
    }
    
    func stopLoading() {
        DispatchQueue.main.async {
            self.loadingView.stopLoading()
        }
    }
    
    func refreshDidEnd() {
        DispatchQueue.main.async {
            if self.refreshControl.isRefreshing {
                self.refreshControl.endRefreshing()
            }
        }
    }
    
    @objc func didPullToRefresh() {
        self.presenter?.fetchData()
    }
    
    // MARK:- Private Utilities
    private func setupViews() {
        self.setupLoadingView()
        self.setupVehiclesList()
    }
    
    private func setupLoadingView() {
        self.view.addSubview(loadingView)
        NSLayoutConstraint.match(child: self.loadingView, parent: self.view)
        self.loadingView.isHidden = false
    }
    
    private func setupVehiclesList() {
        self.view.addSubview(vehiclesList)
        NSLayoutConstraint.match(child: self.vehiclesList, parent: self.view)
        self.vehiclesList.refreshControl = refreshControl
        self.vehiclesList.isHidden = true
        
    }
}
