//
//  VehiclesListPresenter.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit
import DeepDiff


class VehiclesListPresenter: NSObject, Presenter, UITableViewDataSource, UITableViewDelegate {
    let interactor: Interactor
    weak var view: VehiclesListView?
    
    private var viewModels: [VehicleListViewModel] = []
    
    typealias ViewModel = VehicleListViewModel
    
    init(interactor: Interactor) {
        self.interactor = interactor
    }
    
    func fetchData() {
        if viewModels.isEmpty {
            self.view?.showLoadingView()
        }
        
        interactor.fetch()
    }
    
    func stopFetch() {
        interactor.cancelFetch()
    }
    
    func showErrorMessage(_ message: String? = nil) {
        self.view?.refreshDidEnd()
        self.view?.stopLoading()
        self.view?.defaultErrorAlert(message: message)
    }
    
    func didRetrieve(_ viewModels: [VehicleListViewModel]) {
        let changes = diff(old: self.viewModels, new: viewModels)
        self.viewModels = viewModels
        self.view?.refreshDidEnd()
        self.view?.showListView()
        self.view?.reload(changes: changes)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return VehiclesListCell.cellHeight
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: VehiclesListCell.identifier, for: indexPath) as? VehiclesListCell,
            self.viewModels.count >= indexPath.item else { return UITableViewCell() }
        
        let viewModel = self.viewModels[indexPath.item]
        cell.viewModel = viewModel
        cell.selectionStyle = .none
        
        return cell
    }
}
