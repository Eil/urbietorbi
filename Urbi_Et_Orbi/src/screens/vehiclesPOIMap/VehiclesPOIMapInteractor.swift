//
//  VehiclesPOIMapInteractor.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import CoreLocation


protocol Interactor {
    func fetch()
    func cancelFetch()
}

protocol MapInteractor: Interactor {
    func startingCoordinates() -> CLLocationCoordinate2D
}


class VehiclesPOIMapInteractor: MapInteractor {
    let dataProvider: DataProvider
    let city: Urbi.City
    var searchTimer: Timer?
    
    init(dataProvider: DataProvider, city: Urbi.City) {
        self.dataProvider = dataProvider
        self.city = city
    }
    
    weak var presenter: VehiclesPOIMapPresenter?
    var searchInterval: TimeInterval = 20.0
    
    func fetch() {
        self.searchTimer = Timer.scheduledTimer(withTimeInterval: searchInterval, repeats: true, block: { [weak self] _ in
            guard let sself = self else { return }
            sself.fetchVehicles()
        })
        self.searchTimer?.fire()
    }
    
    func cancelFetch() {
        self.dataProvider.cancelFetch()
        searchTimer?.invalidate()
        searchTimer = nil
    }
    
    func startingCoordinates() -> CLLocationCoordinate2D {
        let coordinates = Coordinates()
        return coordinates[city] ?? Coordinates.defaultCoordinates
    }
    
    private func fetchVehicles() {
        dataProvider.fetch { [weak self] (fetchresult: FetchResult<Vehicles>) in
            switch fetchresult {
            case .success(let vehicles):
                let viewModels = vehicles.vehicles.map({ VehicleAnnotationViewModel(vehicle: $0) })
                self?.presenter?.didRetrieve(viewModels)
            case .error(let error):
                guard let taskError = error as? NetworkError else {
                    self?.presenter?.showErrorMessage()
                    return
                }
                self?.onNetworkError(taskError)
            }
        }
    }
    
    private func onNetworkError(_ error: NetworkError) {
        switch error {
        case .taskError(let foundationError):
            guard (foundationError as NSError).code != NSURLErrorCancelled else { return }
            self.presenter?.showErrorMessage()
        default:
            self.presenter?.showErrorMessage()
        }
    }
}
