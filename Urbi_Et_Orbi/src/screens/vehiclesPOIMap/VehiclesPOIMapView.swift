//
//  VehiclesPOIMapView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class VehiclesPOIMapView: UIViewController {
    var presenter: VehiclesPOIMapPresenter?
    
    lazy private var map: MKMapView = {
        let mapView = MKMapView()
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.delegate = self.presenter
        
        return mapView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupView()
        self.setInitialLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter?.fetchData()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        self.presenter?.stopFetch()
    }
    
    private func setupView() {
        self.view.addSubview(self.map)
        NSLayoutConstraint.match(child: self.map, parent: self.view)
    }
    
    private func setInitialLocation() {
        if let coordinates = presenter?.startingCoordinates() {
            self.centerOnRegion(coordinates: coordinates)
        }
    }
    
    func reload(diff: ViewModelDiff<VehicleAnnotationViewModel>) {
        DispatchQueue.main.async {
            _ = diff.removes.map({ self.map.removeAnnotation($0.annotation)})
            _ = diff.adds.map({ self.map.addAnnotation($0.annotation)})
        }
    }
    
    func deselectAnnotation(_ annotation: VehicleAnnotation) {
        self.map.deselectAnnotation(annotation, animated: true)
    }
    
    private func centerOnRegion(coordinates: CLLocationCoordinate2D) {
        self.map.setRegion(coordinates.region(), animated: true)
    }
    
    func centerOnLocation(coordinates: CLLocationCoordinate2D) {
        let regionOfInterest = coordinates.annotation()
        
        if shouldZoomOnRegion(regionOfInterest) {
            self.map.setRegion(regionOfInterest, animated: true)
        } else {
            self.map.setCenter(coordinates, animated: true)
        }
    }
    
    private func shouldZoomOnRegion(_ region: MKCoordinateRegion) -> Bool {
        let expectedRect = region.mapRect()
        
        return (self.map.visibleMapRect.size.height > expectedRect.size.height && self.map.visibleMapRect.size.width > expectedRect.size.width)
    }
}

