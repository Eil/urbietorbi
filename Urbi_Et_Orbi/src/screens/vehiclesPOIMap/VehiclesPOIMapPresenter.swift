//
//  VehiclesPOIMapPresenter.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import MapKit

protocol Presenter: class {
    associatedtype ViewModel
    
    func fetchData()
    func stopFetch()
    func showErrorMessage(_ message: String?)
    func didRetrieve(_ viewModels: [ViewModel])
}

protocol MapPresenter: Presenter {
    func startingCoordinates() -> CLLocationCoordinate2D
}

protocol VehicleDetailViewDelegate: class {
    func detailWillDismiss()
    func onBookingSuccess()
    func onBookingError()
}

class VehiclesPOIMapPresenter: NSObject, MapPresenter, MKMapViewDelegate, VehicleDetailViewDelegate {
    let interactor: MapInteractor
    weak var view: VehiclesPOIMapView?
    
    private var viewModels: [VehicleAnnotationViewModel] = []
    private var selectedAnnotation: VehicleAnnotation?
    
    typealias ViewModel = VehicleAnnotationViewModel
    
    init(interactor: MapInteractor) {
        self.interactor = interactor
    }
    
    func fetchData() {
        interactor.fetch()
    }
    
    func stopFetch() {
        interactor.cancelFetch()
    }
    
    func showErrorMessage(_ message: String? = nil) {
        self.view?.defaultErrorAlert(message: message)
    }
    
    func didRetrieve(_ viewModels: [VehicleAnnotationViewModel]) {
        let diff = ViewModelDiff<VehicleAnnotationViewModel>(from: self.viewModels, to: viewModels)
        self.viewModels = viewModels
        self.view?.reload(diff: diff)
    }
    
    func startingCoordinates() -> CLLocationCoordinate2D {
        return interactor.startingCoordinates()
    }
    
    func detailWillDismiss() {
        guard let annotation = self.selectedAnnotation else { return }
        self.view?.deselectAnnotation(annotation)
        self.selectedAnnotation = nil
    }
    
    func onBookingSuccess() {
        DispatchQueue.main.async {
            self.view?.toast(message: "booking_action_success_message".localized(), duration: 0.75)
        }
    }
    
    func onBookingError() {
        self.showErrorMessage()
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation as? VehicleAnnotation else { return }
        
        self.view?.centerOnLocation(coordinates: annotation.coordinate)
        
        if let view = view as? SelectableAnnotationView {
            view.onSelection()
        }
        
        let detailViewModel = VehicleDetailViewModel(vehicle: annotation.vehicle)
        
        let detailView = VehicleDetailView(viewModel: detailViewModel)
        detailView.delegate = self
        detailView.modalPresentationStyle = .custom
        detailView.modalTransitionStyle = .crossDissolve
        
        self.selectedAnnotation = annotation
        
        self.view?.present(detailView, animated: true, completion: nil)
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard let vehicleAnnotation = annotation as? VehicleAnnotation else { return nil }
        
        return mapView.view(for: vehicleAnnotation)
    }
    
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        if let view = view as? SelectableAnnotationView {
            view.onDeselection()
        }
    }
}
