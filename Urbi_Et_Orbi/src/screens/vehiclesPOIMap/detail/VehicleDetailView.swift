//
//  VehicleDetailView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit


class VehicleDetailView: UIViewController {
    let viewModel: VehicleDetailViewModel
    weak var delegate: VehicleDetailViewDelegate?
    
    private var bookingInProgressToast: UIAlertController?
    
    init(viewModel: VehicleDetailViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private lazy var modelLabel : UILabel = {
        let m = UILabel()
        m.translatesAutoresizingMaskIntoConstraints = false
        m.font = UIFont.boldSystemFont(ofSize: 20.0)
        m.numberOfLines = 0
        
        return m
    }()
    
    private lazy var addressLabel : UILabel = {
        let a = UILabel()
        a.translatesAutoresizingMaskIntoConstraints = false
        a.font = UIFont.systemFont(ofSize: 15.0)
        a.numberOfLines = 0
    
        return a
    }()
    
    private lazy var fuelInfoLabel : UILabel = {
        let f = UILabel()
        f.translatesAutoresizingMaskIntoConstraints = false
        f.font = UIFont.systemFont(ofSize: 15.0)
        f.numberOfLines = 0
        
        return f
    }()
    
    lazy var fuelImage: UIImageView = {
        let f = UIImageView()
        f.translatesAutoresizingMaskIntoConstraints = false
        f.contentMode = .scaleAspectFit
        return f
    }()
    
    private lazy var thumbnail: UIImageView = {
        let t = UIImageView()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.contentMode = .scaleAspectFill
        
        return t
    }()
    
    private lazy var bookButton : UIButton = {
        let b = UIButton()
        b.translatesAutoresizingMaskIntoConstraints = false
        b.contentHorizontalAlignment = .left
        b.setTitle("booking_action_button_text".localized(), for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.titleLabel?.font = UIFont.boldSystemFont(ofSize: 17.0)
        b.backgroundColor = Colors.accentColor
        b.layer.cornerRadius = 5
        b.layer.borderColor = Colors.accentColor.cgColor
        b.clipsToBounds = true
        b.contentHorizontalAlignment = .center
        b.addTarget(self, action: #selector(bookNow), for: .touchUpInside)
        
        return b
    }()
    
    lazy private var infoView: RoundedView = {
        let v = RoundedView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.radius = 12.0
        v.color = .white
        
        return v
    }()
    
    
    lazy private var transparentView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor  = UIColor.black.withAlphaComponent(0.6)
        
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissDetail))
        v.addGestureRecognizer(gestureRecognizer)
        
        return v
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .clear
        setupViews()
        setupInfo()
    }
    
    func setupViews() {
        
        self.view.addSubview(transparentView)
        self.view.insertSubview(infoView, aboveSubview: transparentView)
        
        infoView.addSubview(modelLabel)
        infoView.addSubview(addressLabel)
        infoView.addSubview(fuelInfoLabel)
        infoView.addSubview(thumbnail)
        infoView.addSubview(bookButton)
        infoView.addSubview(fuelImage)
        
        let guide = self.view.safeAreaLayoutGuide
        
        NSLayoutConstraint.activate([
            transparentView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
            transparentView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            transparentView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
            transparentView.topAnchor.constraint(equalTo: guide.topAnchor)
        ])
        
        NSLayoutConstraint.activate([
            infoView.bottomAnchor.constraint(equalTo: guide.bottomAnchor),
            infoView.leadingAnchor.constraint(equalTo: guide.leadingAnchor),
            infoView.trailingAnchor.constraint(equalTo: guide.trailingAnchor),
        ])
        
        NSLayoutConstraint.activate([
            bookButton.bottomAnchor.constraint(equalTo: infoView.bottomAnchor, constant: -8.0),
            bookButton.centerXAnchor.constraint(equalTo: infoView.centerXAnchor),
            bookButton.widthAnchor.constraint(equalTo: infoView.widthAnchor, multiplier: 2/3),
            bookButton.heightAnchor.constraint(equalToConstant: 48.0)
        ])

        NSLayoutConstraint.activate([
            thumbnail.trailingAnchor.constraint(equalTo: infoView.trailingAnchor, constant: -24.0),
            thumbnail.heightAnchor.constraint(equalToConstant: 104.0),
            thumbnail.widthAnchor.constraint(equalToConstant: 104.0),
            thumbnail.bottomAnchor.constraint(equalTo: bookButton.topAnchor, constant: -16.0)
        ])

        NSLayoutConstraint.activate([
            fuelImage.bottomAnchor.constraint(equalTo: thumbnail.bottomAnchor),
            fuelImage.leadingAnchor.constraint(equalTo: infoView.leadingAnchor, constant: 16.0),
            fuelImage.heightAnchor.constraint(equalToConstant: 24.0),
            fuelImage.widthAnchor.constraint(equalToConstant: 24.0)
        ])

        NSLayoutConstraint.activate([
            fuelInfoLabel.bottomAnchor.constraint(equalTo: thumbnail.bottomAnchor),
            fuelInfoLabel.leadingAnchor.constraint(equalTo: fuelImage.trailingAnchor, constant: 8.0),
            fuelInfoLabel.trailingAnchor.constraint(equalTo: thumbnail.leadingAnchor, constant: -16.0),
            fuelInfoLabel.heightAnchor.constraint(equalTo: fuelImage.heightAnchor)
        ])

        NSLayoutConstraint.activate([
            addressLabel.bottomAnchor.constraint(equalTo: fuelInfoLabel.topAnchor, constant: -8.0),
            addressLabel.leadingAnchor.constraint(equalTo: infoView.leadingAnchor, constant: 16.0),
            addressLabel.trailingAnchor.constraint(equalTo: thumbnail.leadingAnchor, constant: -16.0),
            addressLabel.heightAnchor.constraint(equalToConstant: 48.0)
        ])

        NSLayoutConstraint.activate([
            modelLabel.bottomAnchor.constraint(equalTo: addressLabel.topAnchor, constant: -8.0),
            modelLabel.leadingAnchor.constraint(equalTo: infoView.leadingAnchor, constant: 16.0),
            modelLabel.trailingAnchor.constraint(equalTo: thumbnail.leadingAnchor, constant: -16.0),
            modelLabel.heightAnchor.constraint(equalToConstant: 16.0)
        ])

        NSLayoutConstraint.activate([
            infoView.topAnchor.constraint(equalTo: modelLabel.topAnchor, constant: -16.0),
        ])
    }
    
    func setupInfo() {
        self.modelLabel.text = self.viewModel.model
        self.thumbnail.image = self.viewModel.imageName?.icon()
        self.fuelInfoLabel.text = "\(self.viewModel.fuelLevel ?? "?")%"
        self.fuelImage.image = self.viewModel.fuelImageName?.icon()
        
        if let coordinates = self.viewModel.coordinates {
            AddressProvider.address(for: coordinates) { address in
                DispatchQueue.main.async {
                    self.addressLabel.text = address?.defaultFormatting() ?? "address_currently_unavailable".localized()
                }
            }
        }
    }
    
    @objc func dismissDetail() {
        self.dismiss(animated: true, completion: {
            self.delegate?.detailWillDismiss()
        })
    }
    
    @objc func bookNow() {
        guard let id = self.viewModel.id else { return }
        BookingService.default.bookVehicle(vehicleID: id, progress: {
            self.bookingInProgressToast = self.progressToast(message: "booking_action_progress_message".localized())
        }, success: {
            DispatchQueue.main.async {
                self.bookingInProgressToast?.dismiss(animated: true, completion: nil)
                self.dismiss(animated: true, completion: {
                    self.delegate?.detailWillDismiss()
                    self.delegate?.onBookingSuccess()
                })
            }
            
        }) { (_) in
            self.delegate?.onBookingError()
        }
    }
}
