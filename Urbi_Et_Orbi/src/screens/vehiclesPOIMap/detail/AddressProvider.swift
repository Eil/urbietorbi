//
//  AddressProvider.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import CoreLocation

typealias AddressCallback = (Address?) -> Void

class AddressProvider {
    class func address(for coordinates: (latitude: Double, longitude: Double), completion: @escaping AddressCallback) {
        DispatchQueue.global().async {
            let location = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude)
            
            CLGeocoder().reverseGeocodeLocation(location) { placemarks, error in
                guard error == nil, let placemarks = placemarks, !placemarks.isEmpty else {
                    completion(nil)
                    return
                }
                
                let street = placemarks.first?.thoroughfare
                let number = placemarks.first?.subThoroughfare
                let zipcode = placemarks.first?.postalCode
                let locality = placemarks.first?.locality
             
                let address = Address(street: street, number: number, zipCode: zipcode, locality: locality)
                
                completion(address)
            }
        }
    }
}
