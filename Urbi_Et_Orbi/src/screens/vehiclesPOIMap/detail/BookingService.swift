//
//  BookingService.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


protocol VehicleBookingService {
    func bookVehicle(vehicleID: String, progress: () -> Void, success: @escaping () -> Void, failure: @escaping (Error?) -> Void)
}

class FakeBookingService: VehicleBookingService {
    func bookVehicle(vehicleID: String, progress: () -> Void, success: @escaping () -> Void, failure: @escaping (Error?) -> Void) {
        progress()
        
        DispatchQueue.global().asyncAfter(deadline: DispatchTime.now() + 2.0) {
            success()
        }
    }
}


class BookingService {
    
    static let `default`: VehicleBookingService = FakeBookingService()
}
