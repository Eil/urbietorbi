//
//  NSLayoutConstraints+MatchParent.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit


extension NSLayoutConstraint {
    class func match(child: UIView, parent: UIView) {
        child.translatesAutoresizingMaskIntoConstraints = false
        
        let constraints: [NSLayoutConstraint] = [
            child.topAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.topAnchor),
            child.bottomAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.bottomAnchor),
            child.leadingAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.leadingAnchor),
            child.trailingAnchor.constraint(equalTo: parent.safeAreaLayoutGuide.trailingAnchor)
        ]
        
        NSLayoutConstraint.activate(constraints)
    }
}
