//
//  MKMapView+VehicleAnnotation.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import MapKit


extension MKMapView {
    func view(for annotation: VehicleAnnotation) -> MKAnnotationView? {
        var view: MKAnnotationView
        
        switch annotation.vehicle.provider {
        case .mimoto:
            if let existingView = self.dequeueReusableAnnotationView(withIdentifier: MimotoAnnotationView.identifier) {
                existingView.annotation = annotation
                view = existingView
            } else {
                view = MimotoAnnotationView(annotation: annotation)
            }
        case .sharengo:
            if let existingView = self.dequeueReusableAnnotationView(withIdentifier: SharengoAnnotationView.identifier) {
                existingView.annotation = annotation
                view = existingView
            } else {
                view = SharengoAnnotationView(annotation: annotation)
            }
        }
        
        return view
    }
}
