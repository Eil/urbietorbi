//
//  HTTPURLResponse+Invalid.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


extension HTTPURLResponse {
    
    func isError() -> Bool {
        return self.statusCode >= 400
    }
}
