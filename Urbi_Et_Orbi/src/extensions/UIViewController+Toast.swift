//
//  UIViewController+Toast.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit


extension UIViewController {
    
    func toast(message: String?, duration: TimeInterval = 0.5) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
            alert.view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
            alert.view.layer.cornerRadius = 15
            
            
            self.present(alert, animated: true) {
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + duration, execute: {
                    alert.dismiss(animated: true, completion: nil)
                })
            }
        }
    }
    
    func defaultErrorAlert(message: String? = "default_error_alert_message".localized()) {
        DispatchQueue.main.async {
            let action = UIAlertAction(title: "ok_button_text".localized(), style: .cancel, handler: nil)
            let alert = UIAlertController(title: "default_error_alert_title".localized(), message: message, preferredStyle: .alert)
            alert.addAction(action)
            
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @discardableResult
    func progressToast(message: String?) -> UIAlertController {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        alert.view.layer.cornerRadius = 15
        
        let spinner = UIActivityIndicatorView()
        spinner.translatesAutoresizingMaskIntoConstraints = false
        
        alert.view.addSubview(spinner)
        
        NSLayoutConstraint.activate([
            spinner.leadingAnchor.constraint(equalTo: alert.view.leadingAnchor, constant: 16.0),
            spinner.centerYAnchor.constraint(equalTo: alert.view.centerYAnchor),
            spinner.heightAnchor.constraint(equalTo: alert.view.heightAnchor, multiplier: 1/3)
        ])
        
        DispatchQueue.main.async {
            self.present(alert, animated: true, completion: nil)
            spinner.startAnimating()
        }
        
        return alert
    }
}
