//
//  CLLocationCoordinate2D.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

extension CLLocationCoordinate2D {
    func region(span: MKCoordinateSpan = Coordinates.Span.cityLevel) -> MKCoordinateRegion {
        return coordinateRegion(span: span)
    }
    
    func annotation(span: MKCoordinateSpan = Coordinates.Span.annotationLevel) -> MKCoordinateRegion {
        return coordinateRegion(span: span)
    }
    
    private func coordinateRegion(span: MKCoordinateSpan) -> MKCoordinateRegion {
        return MKCoordinateRegion(center: self, span: span)
    }
}
