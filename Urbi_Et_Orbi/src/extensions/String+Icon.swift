//
//  UIImage+Named.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit


extension String {
    func icon() -> UIImage? {
        return UIImage(named: self)
    }
}
