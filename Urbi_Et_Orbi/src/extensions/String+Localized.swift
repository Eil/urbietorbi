//
//  Strings+Localized.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 30/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation

extension String {
    func localized() -> String {
        return NSLocalizedString(self, comment: "")
    }
}
