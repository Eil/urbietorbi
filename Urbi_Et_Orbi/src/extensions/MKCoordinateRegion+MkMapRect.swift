//
//  MKMapRegion+MkMapRect.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import MapKit


extension MKCoordinateRegion {
    func mapRect() -> MKMapRect {
      let topLeft = CLLocationCoordinate2D(
        latitude: self.center.latitude + (self.span.latitudeDelta/2.0),
        longitude: self.center.longitude - (self.span.longitudeDelta/2.0)
      )

      let bottomRight = CLLocationCoordinate2D(
        latitude: self.center.latitude - (self.span.latitudeDelta/2.0),
        longitude: self.center.longitude + (self.span.longitudeDelta/2.0)
      )

        let topLeftMapPoint = MKMapPoint(topLeft)
        let bottomRightMapPoint = MKMapPoint(bottomRight)

      let origin = MKMapPoint(x: topLeftMapPoint.x,
                              y: topLeftMapPoint.y)
      let size = MKMapSize(width: fabs(bottomRightMapPoint.x - topLeftMapPoint.x),
                           height: fabs(bottomRightMapPoint.y - topLeftMapPoint.y))

      return MKMapRect(origin: origin, size: size)
    }
}
