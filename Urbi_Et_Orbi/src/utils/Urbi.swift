//
//  Urbi.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


struct Urbi {
    
    enum City: String {
        case milano
    }
    
    enum Provider: String {
        case mimoto
        case sharengo
        
        static let milanProviders: [Provider] = [
            mimoto,
            sharengo
        ]
    }
}
