//
//  FetchResult.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


enum FetchResult<Value> {
    case success(Value)
    case error(Error)
}
