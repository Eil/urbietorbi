//
//  Coordinates.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation


struct Coordinates {
    
    private let coordinates: [Urbi.City : CLLocationCoordinate2D] = [
        Urbi.City.milano : CLLocationCoordinate2DMake(45.46, 9.19)
    ]
    
    subscript(city: Urbi.City) -> CLLocationCoordinate2D? {
        get {
            return coordinates[city]
        }
    }
    
    static let defaultCoordinates = CLLocationCoordinate2D(latitude: 37.32, longitude: -122.03)
    
    
    struct Span {
        static let cityLevel = MKCoordinateSpan(latitudeDelta: 0.25, longitudeDelta: 0.25)
        static let annotationLevel = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
    }
}


