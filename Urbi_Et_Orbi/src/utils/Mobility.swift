//
//  Mobility.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation

struct Mobility {
    let city: Urbi.City
    let providers: [Urbi.Provider]
    
    static func milan() -> Mobility {
        return Mobility(city: .milano, providers: Urbi.Provider.milanProviders)
    }
}
