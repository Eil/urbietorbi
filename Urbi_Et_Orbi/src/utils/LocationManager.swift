//
//  LocationManager.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import MapKit
import CoreLocation

enum LocationError {
    case generic
    case unauthorized
}


class LocationManager: NSObject, CLLocationManagerDelegate {

    static let shared: LocationManager = LocationManager()
    
    lazy private var manager = CLLocationManager()
    private var handler : ((_ coordinate: CLLocation?, _ locationError: LocationError?) -> Void)?
    
    fileprivate override init() {
        super.init()
        manager.delegate = self
    }
    
    func stop() {
        manager.stopUpdatingLocation()
    }
    
    func getCurrentLocation(handler: @escaping (_ location: CLLocation?, _ locationError: LocationError?) -> Void) {
        self.handler = handler
        
        let status = CLLocationManager.authorizationStatus()
       
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            manager.requestLocation()
        case .notDetermined:
            manager.requestAlwaysAuthorization()
        case .denied, .restricted:
            self.handler = nil
            handler(nil, .unauthorized)
        default:
            self.handler = nil
            handler(nil, .generic)
        }
       
    }

    //MARK: CLLocationManagerDelegate
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            manager.requestLocation()
        case .notDetermined:
            manager.requestAlwaysAuthorization()
        case .denied, .restricted:
            handler?(nil, .unauthorized)
        default:
            handler?(nil, .generic)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let latestLocation = locations.last {
            self.handler?(latestLocation, nil)
            self.handler = nil
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.handler?(nil, .generic)
    }
}
