//
//  Address.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


struct Address {
    let street: String?
    let number: String?
    let zipCode: String?
    let locality: String?
    
    func defaultFormatting() -> String {
        var text: String = ""
        if let street = self.street {
            text += street
            
            if let number = self.number {
                text += ", \(number)"
            }
        }
        
        if let zipcode = self.zipCode {
            if !text.isEmpty {
                text += ", "
            }
            text += zipcode
        }
        
        if let locality = self.locality {
            if !text.isEmpty {
                text += " "
            }
            text += locality
        }
        
        return text
    }
}
