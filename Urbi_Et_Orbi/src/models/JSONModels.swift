//
//  Vehicles.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 26/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation

struct Vehicles: Decodable {
    var location: String
    var vehicles: [Vehicle]
}

enum VehicleType: String, Decodable {
    case car
    case scooter
}

enum VehicleProvider: String, Decodable {
    case mimoto
    case sharengo
}

struct Vehicle: Decodable, Equatable {
    var fuel: Fuel
    var id: String
    var imageUrl: String?
    var licensePlate: String
    var location: Location
    var model: String
    var pinUrl: String
    var provider: VehicleProvider
    var type: VehicleType
    
    static func == (lhs: Vehicle, rhs: Vehicle) -> Bool {
        return lhs.id == rhs.id &&
            lhs.fuel == rhs.fuel &&
            lhs.imageUrl == rhs.imageUrl &&
            lhs.licensePlate == rhs.licensePlate &&
            lhs.location == rhs.location &&
            lhs.model == rhs.model &&
            lhs.pinUrl == rhs.pinUrl &&
            lhs.provider == rhs.provider
    }
}

enum FuelType: String, Decodable {
    case electric
}

struct Fuel: Decodable, Equatable {
    var percent: Int
    var range: Int
    var type: FuelType?
}


struct Location: Decodable, Equatable {
    var lat: Double
    var lon: Double
}
