//
//  UrbiError.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation


struct UrbiError: Error, Decodable {
    var message: String
    var status: Int
    var statusFromProvider: Bool
    var uri: String
}
