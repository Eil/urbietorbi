//
//  VehiclesListCell.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 30/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit


class VehiclesListCell: UITableViewCell {
    static let cellHeight: CGFloat = 96.0
    static let identifier: String = "vehiclesListCell"
    
    private lazy var modelLabel : UILabel = {
        let m = UILabel()
        m.translatesAutoresizingMaskIntoConstraints = false
        m.font = UIFont.boldSystemFont(ofSize: 17.0)
        m.numberOfLines = 0
        
        return m
    }()
    
    private lazy var licensePlateLabel : UILabel = {
        let l = UILabel()
        l.translatesAutoresizingMaskIntoConstraints = false
        l.font = UIFont.boldSystemFont(ofSize: 17.0)
        l.numberOfLines = 0
        
        return l
    }()
    
    private lazy var fuelInfoLabel : UILabel = {
        let f = UILabel()
        f.translatesAutoresizingMaskIntoConstraints = false
        f.font = UIFont.systemFont(ofSize: 15.0)
        f.numberOfLines = 0
        
        return f
    }()
    
    lazy var distanceImageView: UIImageView = {
        let d = UIImageView()
        d.translatesAutoresizingMaskIntoConstraints = false
        d.contentMode = .scaleAspectFit
        return d
    }()
    
    private lazy var distanceInfoLabel : UILabel = {
        let d = UILabel()
        d.translatesAutoresizingMaskIntoConstraints = false
        d.font = UIFont.systemFont(ofSize: 15.0)
        d.numberOfLines = 0
        
        return d
    }()
    
    lazy var fuelImage: UIImageView = {
        let f = UIImageView()
        f.translatesAutoresizingMaskIntoConstraints = false
        f.contentMode = .scaleAspectFit
        return f
    }()
    
    private lazy var thumbnail: UIImageView = {
        let t = UIImageView()
        t.translatesAutoresizingMaskIntoConstraints = false
        t.contentMode = .scaleAspectFill
        
        return t
    }()
    
    lazy private var containerView: UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        return v
    }()
    
    override public init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.backgroundColor = .white
        setupViews()
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.backgroundColor = .white
        setupViews()
    }
    
    override var reuseIdentifier: String? {
        get {
            return VehiclesListCell.identifier
        }
        set {}
    }
    
    var viewModel: VehicleListViewModel? {
        didSet {
            guard let model = viewModel else { return }
            apply(model)
            
        }
    }
    
    private func setupViews() {
        contentView.addSubview(containerView)
        contentView.backgroundColor = .white
        
        containerView.addSubview(modelLabel)
        containerView.addSubview(licensePlateLabel)
        containerView.addSubview(fuelInfoLabel)
        containerView.addSubview(fuelImage)
        containerView.addSubview(distanceImageView)
        containerView.addSubview(distanceInfoLabel)
        containerView.addSubview(thumbnail)
        
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 16.0),
            containerView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor, constant: -16.0),
            containerView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 16.0),
            containerView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -16.0)
        ])
        
        NSLayoutConstraint.activate([
            thumbnail.heightAnchor.constraint(equalTo: containerView.heightAnchor),
            thumbnail.leadingAnchor.constraint(equalTo: containerView.leadingAnchor, constant: 8.0),
            thumbnail.widthAnchor.constraint(equalTo: thumbnail.heightAnchor)
        ])
        
        NSLayoutConstraint.activate([
            modelLabel.topAnchor.constraint(equalTo: thumbnail.topAnchor),
            modelLabel.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            modelLabel.widthAnchor.constraint(equalToConstant: 96.0)
        ])
        
        NSLayoutConstraint.activate([
            distanceImageView.bottomAnchor.constraint(equalTo: thumbnail.bottomAnchor),
            distanceImageView.leadingAnchor.constraint(equalTo: modelLabel.leadingAnchor),
            distanceImageView.widthAnchor.constraint(equalToConstant: 24.0),
            distanceImageView.heightAnchor.constraint(equalToConstant: 24.0)
        ])
        
        NSLayoutConstraint.activate([
            distanceInfoLabel.bottomAnchor.constraint(equalTo: thumbnail.bottomAnchor),
            distanceInfoLabel.leadingAnchor.constraint(equalTo: distanceImageView.trailingAnchor, constant: 8.0),
            distanceInfoLabel.widthAnchor.constraint(equalToConstant: 96.0)
        ])
        
        NSLayoutConstraint.activate([
            fuelInfoLabel.bottomAnchor.constraint(equalTo: thumbnail.bottomAnchor),
            fuelInfoLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -16.0),
        ])
        
        NSLayoutConstraint.activate([
            fuelImage.bottomAnchor.constraint(equalTo: thumbnail.bottomAnchor),
            fuelImage.trailingAnchor.constraint(equalTo: fuelInfoLabel.leadingAnchor, constant: -8.0),
            fuelImage.heightAnchor.constraint(equalToConstant: 24.0),
            fuelImage.widthAnchor.constraint(equalToConstant: 24.0)
        ])
        
        NSLayoutConstraint.activate([
            licensePlateLabel.topAnchor.constraint(equalTo: thumbnail.topAnchor),
            licensePlateLabel.trailingAnchor.constraint(equalTo: containerView.trailingAnchor),
            licensePlateLabel.leadingAnchor.constraint(equalTo: fuelImage.leadingAnchor)
        ])
    }
    
    private func apply(_ viewModel: VehicleListViewModel) {
        self.modelLabel.text = viewModel.model
        self.licensePlateLabel.text = viewModel.licensePlate
        self.thumbnail.image = viewModel.imageName?.icon()
        self.distanceImageView.image = "tab_bar_list".icon()
        self.distanceInfoLabel.text = viewModel.distance
        self.fuelInfoLabel.text = "\(viewModel.fuelLevel ?? "?")%"
        self.fuelImage.image = viewModel.fuelImageName?.icon()
    }
}
