//
//  LoadingView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 30/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit

class LoadingView: UIView {
    
    private let activityIndicator = UIActivityIndicatorView(style: .medium)
    
    private func setup() {
        self.addSubview(activityIndicator)
        activityIndicator.translatesAutoresizingMaskIntoConstraints = false
        
        NSLayoutConstraint.activate([
            activityIndicator.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            activityIndicator.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            activityIndicator.heightAnchor.constraint(equalToConstant: 44.0),
            activityIndicator.widthAnchor.constraint(equalToConstant: 44.0)
        ])
        activityIndicator.startAnimating()
    }
    
    // MARK: - Public APIs
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func startLoading() {
        activityIndicator.startAnimating()
    }
    
    func stopLoading() {
        activityIndicator.stopAnimating()
    }
}
