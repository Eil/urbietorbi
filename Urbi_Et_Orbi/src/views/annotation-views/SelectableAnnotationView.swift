//
//  SelectableAnnotationView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 01/10/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import MapKit


protocol SelectableAnnotationView {
    func onSelection()
    func onDeselection()
}

extension SelectableAnnotationView where Self: MKAnnotationView {
    func onSelection() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.1, animations: {
                self.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
            })
        }
    }
    
    func onDeselection() {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.1, animations: {
                self.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
}


