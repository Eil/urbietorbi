//
//  MimotoAnnotationView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class MimotoAnnotationView: MKAnnotationView, SelectableAnnotationView {
    static let identifier: String = "pin_mimoto_id"
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String? = nil) {
        super.init(annotation: annotation, reuseIdentifier: MimotoAnnotationView.identifier)
        self.image = "pin_mimoto".icon()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
