//
//  SharengoAnnotationView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit
import MapKit


class SharengoAnnotationView: MKAnnotationView, SelectableAnnotationView {
    static let identifier: String = "pin_sharengo_id"
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String? = nil) {
        super.init(annotation: annotation, reuseIdentifier: SharengoAnnotationView.identifier)
        self.image = "pin_sharengo".icon()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
