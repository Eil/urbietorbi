//
//  RoundedView.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import UIKit


class RoundedView : UIView {
    var path: UIBezierPath!
    
    var radius: CGFloat = 0
    var color: UIColor?
    var cornersToRound: UIRectCorner = [.topLeft, .topRight]
    
    
    override var isOpaque: Bool {
        get {
            return false
        }
        set {
            self.isOpaque = newValue
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func draw(_ rect: CGRect) {
        self.path = UIBezierPath(roundedRect: rect, byRoundingCorners: cornersToRound, cornerRadii: CGSize(width: radius, height: radius))
        (color ?? .clear).setFill()
        path.fill()
    }
}
