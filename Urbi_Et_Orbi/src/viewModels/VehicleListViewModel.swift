//
//  VehicleListViewModel.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 30/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import DeepDiff
import UIKit


struct VehicleListViewModel {
    let id: String?
    let model: String?
    let coordinates: (latitude: Double, longitude: Double)?
    let licensePlate: String?
    let imageName: String?
    let fuelLevel: String?
    let fuelImageName: String?
    var distance: String?
    
    init(vehicle: Vehicle) {
        self.model = vehicle.model
        self.coordinates = (vehicle.location.lat, vehicle.location.lon)
        self.licensePlate = vehicle.licensePlate
        self.imageName = "vehicle_\(vehicle.provider.rawValue)"
        self.fuelLevel = "\(vehicle.fuel.percent)"
        self.fuelImageName = "fuel_\(vehicle.fuel.type?.rawValue ?? "")"
        self.id = vehicle.id
    }
}

extension VehicleListViewModel: DiffAware {
    
    typealias DiffId = String?
    
    var diffId: String? {
        return self.id
    }
    
    static func compareContent(_ a: VehicleListViewModel, _ b: VehicleListViewModel) -> Bool {
        return a == b
    }
}

extension VehicleListViewModel: Equatable {
    static func == (lhs: VehicleListViewModel, rhs: VehicleListViewModel) -> Bool {
        return lhs.id == rhs.id &&
            lhs.model == rhs.model &&
            lhs.coordinates?.longitude == rhs.coordinates?.longitude &&
            lhs.coordinates?.latitude == rhs.coordinates?.latitude &&
            lhs.licensePlate == rhs.licensePlate &&
            lhs.fuelLevel == rhs.fuelLevel &&
            lhs.distance == rhs.distance
    }
}
