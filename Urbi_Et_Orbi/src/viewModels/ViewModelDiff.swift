//
//  ViewModelDiff.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 28/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import DeepDiff

struct ViewModelDiff<T: DiffAware> {
    var adds: [T] = []
    var removes: [T] = []
    
    init(from old: [T], to new: [T]) {
        let differ = diff(old: old, new: new)
        
        _ = differ.compactMap({
            switch $0 {
            case .insert(let added):
                adds.append(added.item)
            case .delete(let removed):
                removes.append(removed.item)
            default:
                break
            }
        })
    }
}
