//
//  VehicleListViewModelFactory.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 30/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit


class VehicleListViewModelFactory {
    
    class func model(from vehicle: Vehicle, considering currentLocation: CLLocation?) -> VehicleListViewModel {
        var model = VehicleListViewModel(vehicle: vehicle)
        
        if let coordinates = model.coordinates, let location = currentLocation {
            let rawDistance = CLLocation(latitude: coordinates.latitude, longitude: coordinates.longitude).distance(from: location)
            model.distance = MKDistanceFormatter().string(fromDistance: rawDistance)
        }
        
        return model
    }
}
