//
//  VehicleAnnotationViewModel.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 27/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import MapKit
import DeepDiff

class VehicleAnnotation: NSObject, MKAnnotation {
    let coordinate: CLLocationCoordinate2D
    let vehicle: Vehicle
    
    init(coordindate: CLLocationCoordinate2D, vehicle: Vehicle) {
        self.coordinate = coordindate
        self.vehicle = vehicle
    }
}

struct VehicleAnnotationViewModel {
    let vehicle: Vehicle
    let annotation: VehicleAnnotation
    
    init(vehicle: Vehicle) {
        self.vehicle = vehicle
        let coordinate = CLLocationCoordinate2D(latitude: vehicle.location.lat, longitude: vehicle.location.lon)
        self.annotation = VehicleAnnotation(coordindate: coordinate, vehicle: vehicle)
    }
}

extension VehicleAnnotationViewModel: DiffAware {
    typealias DiffId = String
    
    var diffId: String {
        return self.vehicle.id
    }
    
    static func compareContent(_ a: VehicleAnnotationViewModel, _ b: VehicleAnnotationViewModel) -> Bool {
        return a.vehicle == b.vehicle
    }
}

extension VehicleAnnotationViewModel: Equatable {}
