//
//  VehicleDetailViewModel.swift
//  Urbi_Et_Orbi
//
//  Created by Alessandro Emmi on 29/09/2019.
//  Copyright © 2019 Alessandro Emmi. All rights reserved.
//

import Foundation
import UIKit
import DeepDiff

struct VehicleDetailViewModel {
    let id: String?
    let model: String?
    let coordinates: (latitude: Double, longitude: Double)?
    let imageName: String?
    let fuelLevel: String?
    let fuelImageName: String?
    
    init(vehicle: Vehicle) {
        self.model = vehicle.model
        self.coordinates = (vehicle.location.lat, vehicle.location.lon)
        self.imageName = "vehicle_\(vehicle.provider.rawValue)"
        self.fuelLevel = "\(vehicle.fuel.percent)"
        self.fuelImageName = "fuel_\(vehicle.fuel.type?.rawValue ?? "")"
        self.id = vehicle.id
    }
}
